"use strict"
// Этот код работает в современном режиме


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ  ------------------------
// ------< TASK#1 >------------------------------


// Дополнить функцию createNewUser() методами расчета возраста пользователя и пароля.
// Задание должно быть выполненно на чистом Javascript, без использования библиотек типа jQuery / React.

// Технические требования: 

// Возьмите выполненное домашнее задание №5(созданная Вами функция createNewUser()) и дополните её следующим функционалом:
// При вызове функция должна спросить дату рождения пользователя (текст в формате dd.mm.yyyy) и сохранить её в переменную с поле birthday.
// Создать метод getAge(), который будет возвращать возраст пользователя на данный момент.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с 
// фамилией пользователя (в нижнем регистре) и город его рождения. Пример:  Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вывести в консоль результат работы функции  createNewUser(), а так же функций getAge() и getPassword() созданного объекта.


// АЛГОРИТМ РЕШЕНИЯ ЗАДАЧИ (от ДЗ№5): 

// 1/ Создаём функцию - конструтор для создания нового объекта с 4 базовыми ключами 
//    (не будем создавать литерал, - с конструктором удобнее работать для многоразового использования)
// 2/ Создаем простые функции - которые будут выполнять проверку корректности ввода 
//    и будут использоваться в качестве аргументов (коллбек-функции) в функции конструкторе
// 3/ getLogin()- реализуем через встроенный в функцию-конструктор метод, который будет автоматически доступен для 
//    для каждого экземпляра объекта, созданного через данную функцию-конструктор
// 4/ Инкапсуляцию переменный делаем через локальные переменные в функции - конструкторе
// 5/ Сеттеры и геттеры реализуем через метод Object.defineProperties (так код будет короче и оптимальнее)
// 6/ *** Добавили два новые ключи со значениями через Object.assign
// 7/ *** Вывести в консоль все ключи при помощи Object.keys
// 8/ *** Проверить наличие ключа при помощи оператора in
// 9/ *** Использовать синтаксис - опциональная цепочка
// 10/ *** Использовать цикл for in:
// - *** -  мои усложнения, чтобы потренировать то, что изучил


// АЛГОРИТМ РЕШЕНИЯ ЗАДАЧИ (от ДЗ№6): 

// 1/ Создаем метод валидации даты рождения в нужный формат, который поддерживаетJava Script this.validBirthDate()
// 2/ Создаем метод для вычисления возраста, с учётом наступившего дня рождения this.getAge()
// 3/ Создаём метод для генерирования пароля пользоваталея this.getPassWord()



//  Start ot the TASK#1 ------------------------------------------------------------------------------


//  ХОД ВЫПОЛНЕНИЯ ЗАДАЧИ: 
console.log ('Task #1 < HW6 - user-age-and-password > started  \u{1F680}')

// Создали объект user при помощи функции
const user = new СreateNewUser(askFirstName(), askLastName(), askBirthdate(), askEmail());
// Вывели в консоль новый объект User
console.log(user);

// user.firstName = 'alex'; // проба изменить имя юзера напрямую вызовет сеттер с коллбэк функцией
// user.lastName = 'Ivanov'; // проба изменить фамилию юзера напрямую вызовет сеттер с коллбэк функцией
// user.birthdate = '01.02.1986'; // проба изменить дату рождения напрямую вызовет сеттер с коллбэк функцией
// user.email = 'al@gmail.com';  // проба изменить e-mail юзера напрямую вызовет сеттер с коллбэк функцией

// Вызывам встроенный метод приветствие:
user.greeting();
// Вызываем встроенный метод краткая инфо:
user.showInfo();
// Вызываем встроенный метод getLogin() и getFullName() и вывели в консоль: 
console.log (`The User's ${user.getFullName()} personal login has generated: ${user.getLogin()} \u{2705}!`);
// Выводим в консоль пароль пользователя getPassWord()
console.log(`The User's ${user.getFullName()} personal password created: ${user.getPassWord()}  \u{2705}!`);
// Выводим в консоль возраст пользователя getAge()
console.log(`The User's ${user.getFullName()} current age is: ${user.getAge()}.`); 

// Добавили два новых ключа со значениями (***)  и вывели юзера в консоль:
Object.assign(user, {['bio-rythm']:'late sleeper', ['favorite sport']:{water: 'Swimming in the pool', bike:{ type:'MTB', brand:'Trek', ['wheel size']: 29,} ,['street sport']: 'workout',}});

// Используем метод Object.keys(obj) и выведем в консоль все ключи
console.log(Object.keys(user));
// Используем оператор in
if('firstName' in user){
  console.log(`It's ok the key exists! ${user.getFullName()}`);
} else {
  console.log(`There is no firstname!`)
}
// Используем синтаксис - опциональная цепочка:
if (['favorite sport'] in user){
  console.log(user?.['favorite sport']?.bike?.['wheel size']);
}
// Используем цикл for in:
for( let key in user){
  console.log(key);
  // console.log(user[key]);
}

// Конец задачи
console.log ('Task #1 < HW6 - user-age-and-password > finished!:) \u{1F60E} \u{1F3C1}')




//- ОСНОВНАЯ ФУНКЦИЯ ДЛЯ СОЗДАНИЯ ОБЪЕКТА ------------------------------------------------------------
// СОЗДАЁМ ФУНКЦИЮ-КОНСТРУКТОР для создания пустого объекта:  
// Функция содержит 4 параметра, аргументами для которых будут функции ask с проверкой валидности вводых данных

function СreateNewUser(firstName, lastName, birthdate, email) {
  // Базовые свойства юзера 
  // С помощью локальных переменных, которые видны только в функции-конструкторе, искапсулируем свойства 
  // для защиты от изменения извне напрямую, - только через сеттеры
  // можно добавить в тело функции код const = newUser чтобы она возвращала в итоге объект newUser, но так универсальнее

  this._firstName = firstName;  // приватная переменная 
  this._lastName = lastName;    // приватная переменная
  this._birthdate = birthdate;  // приватная переменная
  this._email = email;          // приватная переменная

  Object.defineProperties(this, {

    firstName: {
      get: firstName = () => {return this._firstName},
      set: firstName = () => {
        alert(`To change user First name, follow this \u{27A1}`);
        this._firstName = askFirstName()}, // вызывает функцию-коллбек для верификации ввода
    },

    lastName:{
      get: lastName = () => {return this._lastName},
      set: lastName = () => {
        alert(`To change user Last name, follow this \u{27A1}`);
        this._lastName = askLastName()}, // вызывает функцию-коллбек для верификации ввода
    },

    birthdate:{
      get: birthdate = () => {return this._birthdate},
      set: birthdate = () => {
        alert(`To change user Birthdate, follow this \u{27A1}`);
        this._birthdate = askBirthdate()}, // вызывает функцию-коллбек для верификации ввода
    },

    email:{
      get: email = () => {return this._email},
      set: email = () => {
        alert(`To change user E-mail, follow this \u{27A1}`);
        this._email = askEmail()}, // вызывает функцию-коллбек для верификации ввода
    },
 
  });


  // Дополняем объект встроенными методами юзера
    this.getFullName = () => `${this._firstName} ${this._lastName}`; // возвращает полное имя юзера
    this.validBirthDate =() => {//метод для валидации даты в нужный формат, из формат, который не распознаётся по умолчанию Java Script 
      const partsDate = this._birthdate.split('.'); // разбиваем на части формат даты
      return new Date(partsDate[2], partsDate[1] - 1, partsDate[0]); // приводим в необходимый формат даты 
    };
    this.getAge = () => { // возвращает возраст
      let age = new Date().getFullYear() - this.validBirthDate().getFullYear(); // преобразуем даты в объекты Date
      if(new Date().getMonth() < this.validBirthDate().getMonth() || (new Date().getMonth() === this.validBirthDate().getMonth() && new Date().getDate() < this.validBirthDate())) {
        age--; //делаем проверку - если день рождения еще не наступил - уменьшаем возраст на единицу
      }
      return age; // если наступил - выводим возраст из разницы дат
    } 
    this.getLogin  = () => this._firstName[0].toLowerCase() + this._lastName.toLowerCase(); // Возвращает логин юзера 
    this.getPassWord = () => this._firstName[0].toUpperCase() + this._lastName.toLowerCase() + this.validBirthDate().getFullYear();
    this.greeting = () => {  // приветствие о создании нового аккаунта
      alert(`Hi ${this._firstName} ${this._lastName}!\nYou are welcome!\nYou created new user account \u{2705}!`);
      console.log(`Hi ${this._firstName} ${this._lastName}! You are welcome! You created new user account \u{2705}!`);
    };
    this.showInfo = () => console.log (`Your brief new User info:\n${this._firstName}, ${this._lastName}, ${this._birthdate}, personal e-mail: ${this._email} \u{2705}.`);
  };



//- CALLBACK-ФУНКЦИИ ДЛЯ ПРОВЕРКИ ВВОДА ------------------------------------------------------------
// Эти функции будут callback - функциями, которые будут аргументами в основной функции - конструкторе


// -- ФУНКЦИЯ №1 запрашивает имя пользователя на латинице или кириллице, с проверкой на корретность внесённых данных
// - возвращает имя пользователя, обрезая все пробелы
function askFirstName() {
  let name = "";
  while(!name || !/^[a-zA-Z]+$/.test(name) && !/^[а-яА-Я]+$/.test(name)|| name.length < 2) {
    name = prompt('Please, enter your First name\nUse only Latin or Cyrillic letters, without numbers and spaces');

    if(!name){
      alert ('Sorry\u{203C} \u{26D4}\nUser First name could not be empty\u{2757}');
    } else if (!/^[a-zA-Z]+$/.test(name) && !/^[а-яА-Я]+$/.test(name)){
      alert ('Sorry\u{203C} \u{26D4}\nUser First name may contain Latin or Cyrillic letters only,\nwithout numbers, points, spaces and other characters\u{2757}');
    } else if (name.length < 2){
      alert ('Sorry\u{203C} \u{26D4}\nUser First name may contain 2 or more than 2 letters only!\u{2757}');
    }
  }
  return name.trim();
}

// -- ФУНКЦИЯ №2 запрашивает фамилию пользователя на латинице или кириллице, с проверкой на корретность внесённых данных
// - возвращает фамилию пользователя, обрезая все пробелы
function askLastName() {
  let surname = "";
  while(!surname || !/^[a-zA-Z]+$/.test(surname) && !/^[а-яА-Я]+$/.test(surname)|| surname.length < 2) {
    surname = prompt('Please, enter your Last name\nUse only Latin or Cyrillic letters, without numbers and spaces');

    if(!surname){
      alert ('Sorry\u{203C} \u{26D4}\nLast name could not be empty\u{2757}');
    } else if (!/^[a-zA-Z]+$/.test(surname) && !/^[а-яА-Я]+$/.test(surname)){
      alert ('Sorry\u{203C} \u{26D4}\nLast name may contain Latin or Cyrillic letters only,\nwithout numbers, points, spaces and other characters\u{2757}');
    } else if (surname.length < 2){
      alert ('Sorry\u{203C} \u{26D4}\nLast name may contain 2 or more than 2 letters!\u{2757}');
    }
  }
  return surname.trim();
}

// -- ФУНКЦИЯ №3 запрашивает дату рождения пользователя, с проверкой на корретность внесённых данных
// - возвращает дату рождения пользователя, обрезая все пробелы
function askBirthdate() {
  let birthdate = "";
  while(!birthdate || !/^\d{2}.\d{2}.\d{4}$/.test(birthdate)) {
    birthdate = prompt('Please, enter your Birthdate \u{1F4C5}\nUse the following format: DD.MM.YYYY');

    if(!birthdate){
      alert ('Sorry\u{203C} \u{26D4}\nBirthdate could not be empty\u{2757}');
    } else if (!/^\d{2}.\d{2}.\d{4}$/.test(birthdate)){
      alert ('Sorry\u{203C} \u{26D4}\nBirthdate must be in following format: DD.MM.YYYY,\nwithout points, spaces and other characters\u{2757}');
    }
  }
  return birthdate.trim();
}

// -- ФУНКЦИЯ №4 запрашивает email пользователя, с проверкой на корретность внесённых данных
// - возвращает e-mail пользователя, обрезая все пробелы

function askEmail() {
  let email = "";
  while(!email  || !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)|| email.length < 5) {
    email = prompt('Please, enter your personal E-mail \u{0040}');

    if(!email){
      alert ('Sorry\u{203C} \u{26D4}\nUser E-mail could not be empty\u{2757}');
    } else if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email )){
      alert ('Sorry\u{203C} \u{26D4}\nUser E-mail must be in correct format\u{2757}\nExample: i.zuzin2012@gmail.com');
    }
  }
  return email.trim();
}

// ------< TASK#1 >------ЗАВЕРШЕН------------------------




//-----------------ЗАМЕТКИ ДЛЯ МЕНЯ---------------------------------------

//----

// ВАРИАНТ РЕАЛИЗАЦИИ сеттеров и терреров через метод  Object.defineProperty:
// создаем геттеры и сеттеры для инкапсулированных свойств объекта:
  // Object.defineProperty(this, 'firstName', {
  //   get: firstName = () => {return this._firstName},
  //   set: firstName = () => {this._firstName = askName()},
  // });

  // Object.defineProperty(this, 'lastName', {
  //   get: lastName = () => {return this._lastName},
  //   set: lastName = () => {this._lastName = askSurName()},
  // });

  // Object.defineProperty(this, 'birthdate', {
  //   get: birthdate = () => {return this._birthdate},
  //   set: birthdate = () => {this._birthdate = askBirthdate()},
  // });

  // Object.defineProperty(this, 'email', {
  //   get: email = () => {return this._email},
  //   set: email = () => {this._email = askEmail()},
  // });


//---- 

// Метод trim() возвращает строку с вырезанными пробельными символами с её концов. 
// Метод trim() не изменяет значение самой строки.
//a.trim().length === 0 


// --- примеры для меня: 
// function Person(name, age) {
//   Object.defineProperty(this, "name", {
//     value: name,
//     writable: false
//   });
  
//   Object.defineProperty(this, "age", {
//     get: function() {
//       return age;
//     },
//     set: function(newAge) {
//       if (newAge >= 0 && newAge <= 120) {
//         age = newAge;
//       } else {
//         throw new Error("Age must be between 0 and 120");
//       }
//     }
//   });
// }

// var person1 = new Person("John", 30);
// console.log(person1.name); // выводит "John"
// console.log(person1.age); // выводит "30"
// person1.age = 35; // устанавливает новое значение для возраста
// console.log(person1.age); // выводит "35"
// person1.age = -5; // выбрасывает исключение, так как возраст меньше 0

//--------- вариант 
// function createNewUser(name, surname, email, age) {
//   const newUser = {
//     name: name,
//     surname: surname,
//     email: email,
//     age: age
//   };
//   return newUser;
// }
// console.log(createNewUser('Ivan', 'Ziuzin', 'sdfsdf@sd', 45);